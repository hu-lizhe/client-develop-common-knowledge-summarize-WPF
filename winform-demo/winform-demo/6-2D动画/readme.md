﻿# 2D动画
虽然winform中用到动画的地方并不多，但我认为还是要了解学习一下的。这一节，我会制作一个
转圈式的加载动画，作为demo模板。绘制动画有以下几种方式：  
1. 使用ImageAnimator类封装实现（较简单，但需要gif图片）
2. 使用GDI+（较复杂，需要自己写绘图逻辑）  

这里我们只采用GDI+的方式绘图，绘图API的使用方式可参考
[这里](https://www.cnblogs.com/si-shaohua/archive/2011/11/27/2265159.html)，
对ImageAnimator有兴趣的可自行了解，这里不进行demo演示。