﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace winform_demo._6_2D动画
{
    public partial class TestForm13 : Form
    {
        private int running = 0;

        public TestForm13()
        {
            InitializeComponent();
        }

        private void TestForm13_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            //重置画布
            g.ResetTransform();
            //将画布的参考系原点移至窗体中心
            g.TranslateTransform(this.Width / 2, this.Height / 2);
            //画一个中心点
            Point center = new Point(0,0);
            DrawPoint(g, center,1);
            //定义半径
            int radius = 100;
            //先画点，再将画布旋转30度，剩下的点同理，因为是旋转30度，所有总共需要画12个点
            for (int i = 0; i < 12; i++)
            {
                Point p2 = new Point(center.X - radius, center.Y);
                if (i == running) //如果要画的点正好是在跑的点，则填充并放大
                {
                    DrawPoint(g, p2, 15,Brushes.Black);
                }
                else DrawPoint(g, p2,10);
                g.RotateTransform(30);
            }
        }

        private void DrawPoint(Graphics g,Point point,int radius)
        {
            Rectangle circleRect = new Rectangle(point.X - radius, point.Y - radius, radius * 2, radius * 2);
            Pen pen = new Pen(Color.Black, 1);
            g.DrawEllipse(pen, circleRect);
        }

        private void DrawPoint(Graphics g, Point point, int radius,Brush fillColor)
        {
            Rectangle circleRect = new Rectangle(point.X - radius, point.Y - radius, radius * 2, radius * 2);
            Pen pen = new Pen(Color.Black, 1);
            g.DrawEllipse(pen, circleRect);
            g.FillEllipse(fillColor, circleRect);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            running = (running + 1) % 12;
            this.Invalidate();
        }

        private void TestForm13_Load(object sender, EventArgs e)
        {
            timer1.Interval = (int)numericUpDown1.Value;
            timer1.Enabled = true;
            timer1.Start();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int)numericUpDown1.Value;
        }
    }
}
