﻿using System;
using System.Windows.Forms;
using winform_demo._1_窗体及控件的生命周期;
using winform_demo._2_页_界_面跳转.TestDemo;
using winform_demo._3_熟悉常用的控件.demo;
using winform_demo._4_自定义控件与用户控件.demo;
using winform_demo._5_控件间的数据传递方式.demo1;
using winform_demo._5_控件间的数据传递方式.demo2;
using winform_demo._5_控件间的数据传递方式.demo3;
using winform_demo._5_控件间的数据传递方式.EventUtils;
using winform_demo._6_2D动画;

namespace winform_demo
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //在这里替换要测试的窗体
            Application.Run(new TestForm1());
        }
    }
}
