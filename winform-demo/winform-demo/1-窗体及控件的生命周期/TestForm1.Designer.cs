﻿
namespace winform_demo._1_窗体及控件的生命周期
{
    partial class TestForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            System.Console.WriteLine("Dispose");
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // TestForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "TestForm1";
            this.Text = "TestForm1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TestForm1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TestForm1_FormClosed);
            this.Load += new System.EventHandler(this.TestForm1_Load);
            this.Shown += new System.EventHandler(this.TestForm1_Shown);
            this.VisibleChanged += new System.EventHandler(this.TestForm1_VisibleChanged);
            this.ResumeLayout(false);

        }

        #endregion
    }
}