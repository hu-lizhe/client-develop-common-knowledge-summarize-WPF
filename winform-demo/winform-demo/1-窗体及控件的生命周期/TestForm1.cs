﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform_demo._1_窗体及控件的生命周期
{
    public partial class TestForm1 : Form
    {
        int testNumber = 100;

        public TestForm1()
        {
            InitializeComponent();
            this.Disposed += TestForm1_Disposed;
            Console.WriteLine("TestForm1构造函数" + testNumber);
        }

        private void TestForm1_Disposed(object sender, EventArgs e)
        {
            Console.WriteLine("TestForm1_Disposed" + testNumber);
        }

        private void TestForm1_Load(object sender, EventArgs e)
        {
            Console.WriteLine("TestForm1_Load" + testNumber);
        }

        private void TestForm1_Shown(object sender, EventArgs e)
        {
            Console.WriteLine("TestForm1_Shown" + testNumber);
        }

        private void TestForm1_VisibleChanged(object sender, EventArgs e)
        {
            Console.WriteLine("TestForm1_VisibleChanged" + testNumber);
        }

        private void TestForm1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Console.WriteLine("TestForm1_FormClosing" + testNumber);
        }

        private void TestForm1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Console.WriteLine("TestForm1_FormClosed" + testNumber);
        }
    }
}
