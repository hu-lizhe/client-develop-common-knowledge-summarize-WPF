﻿# 窗体及控件的生命周期  
&nbsp;&nbsp;上手一个新框架时，了解窗体及控件的生命周期是非常重要的，有些框架里窗体和控件的生命周期一致，这需要你上手新框架时再去发现和总结。
我将在这一节通过打印调试法了解winform框架中窗体及控件的生命周期。

## 窗体的生命周期
(注：有关窗体的生命周期代码均在TestForm1窗体中)
- TestForm1构造函数
- TestForm1_Load
- TestForm1_VisibleChanged
- TestForm1_Shown
- TestForm1_FormClosing
- TestForm1_FormClosed

在窗体生命周期内是否能一直访问到私有成员变量？我们在testform1中定义一个成员变量testNumber=100，来看下在生命周期内的访问表现：
- TestForm1构造函数100
- TestForm1_Load100
- TestForm1_VisibleChanged100
- TestForm1_Shown100
- TestForm1_FormClosing100
- TestForm1_FormClosed100

事实证明，在窗体生命周期内能一直访问到私有成员变量。如果窗体类内存在与线程/定时器相关的私有成员变量，则在窗体关闭时需要销毁线程/关闭定时器，避免内存泄漏(如访问已释放的对象异常)

## 控件的生命周期
(注：有关控件的生命周期代码均在TestForm2窗体中)  
控件不提供load，FormClosing，FormClosed事件，或者说，只有窗体才有这些事件，所以，
我们需要根据官网文档来找到控件基类身上有哪些与生命周期相关的事件。  
由于一些事件不好测试，因此基于UserControl自定义一个控件，在此控件上绑定事件测试。测试结果如下：
- MyControl构造函数
- MyControl_HandleCreated
- MyControl_HandleDestroyed
- MyControl_Disposed

其实窗体身上也带有这些事件（因为窗体也继承自Control类），但一般我们用窗体时都是在窗体特有的事件里做事情，比如load，基本不会用控件的生命周期事件。

## winform特有
问题：窗体的OnPaint方法和Paint事件什么关系，谁先执行谁后执行？  
答：OnPaint是一个可重写的方法，在父类里这个方法会触发Paint事件，所以
我们要注意，重写OnPaint方法时，一定要先写base.OnPaint才会触发Paint事件。  

问题：窗体的OnDispose事件和Dispose方法什么关系，谁先执行谁后执行？  
答：与上一个问题的答案类似，Dispose方法也是个被重写的方法，方法里最后会调用
base.Dispose触发OnDispose事件。