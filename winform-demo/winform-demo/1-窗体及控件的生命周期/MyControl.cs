﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform_demo._1_窗体及控件的生命周期
{
    public partial class MyControl : UserControl
    {
        public MyControl()
        {
            InitializeComponent();
            this.HandleCreated += MyControl_HandleCreated;
            this.Disposed += MyControl_Disposed;
            this.HandleDestroyed += MyControl_HandleDestroyed;
            Console.WriteLine("MyControl构造函数");
        }

        private void MyControl_HandleDestroyed(object sender, EventArgs e)
        {
            Console.WriteLine("MyControl_HandleDestroyed");
        }

        private void MyControl_Disposed(object sender, EventArgs e)
        {
            Console.WriteLine("MyControl_Disposed");
        }

        private void MyControl_HandleCreated(object sender, EventArgs e)
        {
            Console.WriteLine("MyControl_HandleCreated");
        }
    }
}
