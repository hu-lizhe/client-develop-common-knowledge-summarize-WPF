﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace winform_demo._1_窗体及控件的生命周期
{
    public partial class TestForm2 : Form
    {
        bool clicked = false;
        MyControl myCtrl;

        public TestForm2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!clicked)
            {
                myCtrl = new MyControl();
                myCtrl.Location = new Point(200, 200);
                myCtrl.Size = new Size(200, 60);
                myCtrl.BackColor = Color.Blue;
                this.Controls.Add(myCtrl);
                button1.Text = "点击销毁控件";
                this.Update();
            }
            else
            {
                this.Controls.Remove(myCtrl);
                myCtrl.Dispose();
                myCtrl = null;
                button1.Text = "点击创建控件";
                this.Update();
            }

            clicked = !clicked;
        }
    }
}
