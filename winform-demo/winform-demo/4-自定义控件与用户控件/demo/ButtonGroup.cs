﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform_demo._4_自定义控件与用户控件.demo
{
    /*
     * ButtonGroup功能：
     * 1. 其中一个按钮被按下时，组内的其他按钮均回到未按下状态；被按下的按钮在按一次时，回归至未按下状态，并触发value取消选中事件
     * 2. 每个按钮已按下和未按下状态的UI表现一致
     * 3. 在设计器中，可拖拽出该控件，然后可设置每个按钮代表的value，按下某个按钮时，触发value选中事件
     * 4. 可设置整个控件的长度和宽度，内部的按钮进行大小自适应
     */
    public partial class ButtonGroup : UserControl
    {
        public delegate void ValueSelectedEventHandler(string value);
        public event ValueSelectedEventHandler ValueSelected;
        public event ValueSelectedEventHandler ValueUnSelected;


        private string[] items = new string[3];

        public Image PressedImage { get; set; }
        public Image UnpressedImage { get; set; }
        public Color PressedColor { get; set; } = SystemColors.ControlDark;
        public Color UnpressedColor { get; set; } = SystemColors.Control;
        public Color CellBorderColor { get; set; }
        public Color MouseDownBackColor { get; set; }
        public Color MouseOverBackColor { get; set; }

        public string[] Items
        {
            set
            {
                items = value;
                UpdateControls();
            }
            get { return items; } 
        }

        public ButtonGroup()
        {
            InitializeComponent();
        }

        private void UpdateControls()
        {
            tableLayoutPanel1.Controls.Clear();
            //依次创建变色按钮并加入到单元格中
            for (int i = 0; i < items.Length; i++)
            {
                var btn = new MyButton();
                btn.Text = items[i];
                btn.Dock = DockStyle.Fill;
                btn.PressedImage = PressedImage;
                btn.UnpressedImage = UnpressedImage;
                btn.PressedColor = PressedColor;
                btn.UnpressedColor = UnpressedColor;
                btn.FlatStyle = FlatStyle.Flat;
                btn.FlatAppearance.BorderColor = CellBorderColor;
                btn.FlatAppearance.BorderSize = 1;
                btn.FlatAppearance.MouseDownBackColor = MouseDownBackColor;
                btn.FlatAppearance.MouseOverBackColor = MouseOverBackColor;
                btn.Cursor = Cursors.Hand;
                btn.StateChanged += (state) =>
                {
                    //当该按钮按下时，将其他按钮置为未按下状态
                    if (state == 1)
                    {
                        ValueSelected?.Invoke(btn.Text);
                        foreach (MyButton mbtn in tableLayoutPanel1.Controls)
                        {
                            if (mbtn.Text == btn.Text)
                            {
                                continue;
                            }
                            mbtn.State = 0;
                        }
                    }
                    else if (state == 0)
                    {
                        ValueUnSelected?.Invoke(btn.Text);
                    }
                };
                tableLayoutPanel1.Controls.Add(btn);
                tableLayoutPanel1.SetColumn(btn, i);
                tableLayoutPanel1.SetRow(btn, 0);
            }
        }

        private void ButtonGroup_Paint(object sender, PaintEventArgs e)
        {
            //根据当前控件的长度和items数组长度，自动调整网格布局控件大小及内部的单元格大小
            int cellCount = items.Length;
            int cellWidth = this.Width / cellCount;
            int cellWidthLeft = this.Width % cellCount;  //控件长度不一定能整除单元长度

            tableLayoutPanel1.Width = this.Width - cellWidthLeft;
            tableLayoutPanel1.Height = this.Height;
            tableLayoutPanel1.Location = new Point(cellWidthLeft / 2, tableLayoutPanel1.Location.Y);

            tableLayoutPanel1.ColumnStyles.Clear();
            tableLayoutPanel1.ColumnCount = cellCount;

            for (int i = 0; i < cellCount; i++)
            {
                tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(
                    SizeType.Percent, cellWidth * 1.0f / tableLayoutPanel1.Width));
            }
        }
    }
}
