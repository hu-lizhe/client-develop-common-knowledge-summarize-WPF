﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform_demo._4_自定义控件与用户控件.demo
{
    public partial class TestForm8 : Form
    {
        public TestForm8()
        {
            InitializeComponent();
        }

        private void buttonGroup1_ValueSelected(string value)
        {
            MessageBox.Show($"您选中的按钮为{value}");
        }

        private void buttonGroup1_ValueUnSelected(string value)
        {
            MessageBox.Show($"您取消选中的按钮为{value}");
        }
    }
}
