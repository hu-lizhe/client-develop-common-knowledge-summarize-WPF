﻿namespace winform_demo._4_自定义控件与用户控件.demo
{
    partial class TestForm8
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.myButton1 = new winform_demo._4_自定义控件与用户控件.demo.MyButton();
            this.buttonGroup1 = new winform_demo._4_自定义控件与用户控件.demo.ButtonGroup();
            this.SuspendLayout();
            // 
            // myButton1
            // 
            this.myButton1.Location = new System.Drawing.Point(330, 89);
            this.myButton1.Name = "myButton1";
            this.myButton1.PressedColor = System.Drawing.SystemColors.ControlDark;
            this.myButton1.PressedImage = null;
            this.myButton1.Size = new System.Drawing.Size(117, 44);
            this.myButton1.State = 0;
            this.myButton1.TabIndex = 1;
            this.myButton1.Text = "myButton1";
            this.myButton1.UnpressedColor = System.Drawing.SystemColors.Control;
            this.myButton1.UnpressedImage = null;
            this.myButton1.UseVisualStyleBackColor = true;
            // 
            // buttonGroup1
            // 
            this.buttonGroup1.CellBorderColor = System.Drawing.Color.Empty;
            this.buttonGroup1.Items = new string[] {
        "西瓜",
        "桃子",
        "李子"};
            this.buttonGroup1.Location = new System.Drawing.Point(252, 214);
            this.buttonGroup1.MouseDownBackColor = System.Drawing.Color.Empty;
            this.buttonGroup1.MouseOverBackColor = System.Drawing.Color.Empty;
            this.buttonGroup1.Name = "buttonGroup1";
            this.buttonGroup1.PressedColor = System.Drawing.SystemColors.ControlDark;
            this.buttonGroup1.PressedImage = null;
            this.buttonGroup1.Size = new System.Drawing.Size(263, 40);
            this.buttonGroup1.TabIndex = 2;
            this.buttonGroup1.UnpressedColor = System.Drawing.SystemColors.Control;
            this.buttonGroup1.UnpressedImage = null;
            this.buttonGroup1.ValueSelected += new winform_demo._4_自定义控件与用户控件.demo.ButtonGroup.ValueSelectedEventHandler(this.buttonGroup1_ValueSelected);
            this.buttonGroup1.ValueUnSelected += new winform_demo._4_自定义控件与用户控件.demo.ButtonGroup.ValueSelectedEventHandler(this.buttonGroup1_ValueUnSelected);
            // 
            // TestForm8
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonGroup1);
            this.Controls.Add(this.myButton1);
            this.Name = "TestForm8";
            this.Text = "TestForm8";
            this.ResumeLayout(false);

        }

        #endregion
        private MyButton myButton1;
        private ButtonGroup buttonGroup1;
    }
}