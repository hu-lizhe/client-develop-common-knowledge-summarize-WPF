﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winform_demo._2_页_界_面跳转
{
    [Flags]
    public enum Authority
    {
        ROOT = 0b10000,
        ADMIN = 0b01000,
        VIP = 0b00100,
        USER = 0b00010,
        VISITOR = 0b00001
    }
}
