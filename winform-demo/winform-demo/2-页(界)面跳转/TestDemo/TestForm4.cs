﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace winform_demo._2_页_界_面跳转.TestDemo
{
    public partial class TestForm4 : Form,IPage
    {
        private int number = 0;

        public bool Cached { get; set; }
        public Authority Authority { get; set; }
        public int Number
        {
            set
            {
                number = value;
                label1.Text = number.ToString();
            }
            get
            {
                return number;
            }
        }

        public TestForm4()
        {
            InitializeComponent();
            Cached = true;
            Authority = Authority.VISITOR;
        }

        public void Pause()
        {
            timer1.Stop();
        }

        public void Restore()
        {
            timer1.Start();
        }

        public void Reset()
        {
            Number = 0;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Number++;
        }

        private void TestForm4_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            timer1.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var paramMap = new Dictionary<string, object>();
            paramMap.Add("name", "TestForm---5");
            paramMap.Add("CurTime", DateTime.Now);
            PageManager.NavigateTo<TestForm5>(paramMap);   //带参数跳转
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PageManager.NavigateTo<TestForm6>();  //不带参数跳转
        }
    }
}
