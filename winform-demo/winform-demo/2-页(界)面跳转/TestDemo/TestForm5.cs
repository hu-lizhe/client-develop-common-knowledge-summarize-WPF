﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform_demo._2_页_界_面跳转.TestDemo
{
    public partial class TestForm5 : Form,IPage
    {
        private string name;
        private int number = 0;
        public bool Cached { get; set; }
        public Authority Authority { get; set; }
        public int Number
        {
            set
            {
                number = value;
                label1.Text = number.ToString();
            }
            get
            {
                return number;
            }
        }
        public DateTime CurTime
        {
            set
            {
                lbl_time.Text = value.ToString();
            }
        }

        public TestForm5()
        {
            InitializeComponent();
            Cached = false;
            Authority = Authority.VIP;
        }

        public void Pause()
        {
            timer1.Stop();
        }

        public void Restore()
        {

        }

        public void Reset()
        {
            Number = 0;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Number++;
        }

        private void TestForm5_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            timer1.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lbl_name.Text = name;
        }
    }
}
