﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform_demo._2_页_界_面跳转.TestDemo
{
    public partial class TestForm3 : Form
    {
        public TestForm3()
        {
            InitializeComponent();
            //TestForm3作为主界面，假设使用者权限为VIP+USER+VISITOR
            PageManager.Role = Authority.VIP | Authority.USER | Authority.VISITOR;
            //将主界面里的panel作为容器
            PageManager.SetContainer(panel1);
            //TestForm4，TestForm5，TestForm6为用于测试的子页面，每个页面的设置如下：
            //TestForm4：有缓存，访问权限VISITOR，页面上有一个数字，每过一秒数字+1，该页面被缓存时，功能暂停
            //TestForm5，无缓存，访问权限VIP，与TestForm4功能相同，但返回到该页面时数字重新开始计时，另外，跳转至该窗体时传入参数Name和当前时间
            //TestForm6，无缓存，访问权限ADMIN，页面上显示一个标题“此为管理员页面”

            //注册子页面
            PageManager.RegisterPage<TestForm4>(defaultPage:true);
            PageManager.RegisterPage<TestForm5>();
            PageManager.RegisterPage<TestForm6>();
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            PageManager.Back();
        }

        private void btn_go_Click(object sender, EventArgs e)
        {
            PageManager.GoBackBack();
        }

        private void TestForm3_Load(object sender, EventArgs e)
        {
            PageManager.Flush();
        }
    }
}
