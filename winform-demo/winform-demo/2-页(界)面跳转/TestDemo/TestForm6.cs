﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform_demo._2_页_界_面跳转.TestDemo
{
    public partial class TestForm6 : Form,IPage
    {
        public bool Cached { get; set; }
        public Authority Authority { get; set; }

        public TestForm6()
        {
            InitializeComponent();
            Authority = Authority.ADMIN;
        }

        public void Restore()
        {
            
        }

        public void Pause()
        {

        }

        public void Reset()
        {

        }
    }
}
