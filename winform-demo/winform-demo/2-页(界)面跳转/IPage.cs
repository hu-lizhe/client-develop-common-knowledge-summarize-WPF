﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winform_demo._2_页_界_面跳转
{
    public interface IPage
    {
        bool Cached { get; set; }
        Authority Authority { get; set; }
        /// <summary>
        /// 页面跳转时如果当前页面作为旧页面，会调用该方法
        /// </summary>
        void Pause();
        /// <summary>
        /// 页面跳转时如果当前页面作为新页面并且设置了缓存，会调用该方法
        /// </summary>
        void Restore();
        /// <summary>
        /// 页面跳转时如果当前页面作为新页面并且设置不缓存，会调用该方法
        /// </summary>
        void Reset();
    }
}
