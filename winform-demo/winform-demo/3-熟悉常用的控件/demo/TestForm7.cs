﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace winform_demo._3_熟悉常用的控件.demo
{
    public partial class TestForm7 : Form
    {
        public Guid ID { get; set; }
        public string NickName { get; set; }
        public string Password { get; set; }
        public int Gender { get; set; }
        public List<string> Hobby;
        public string PersonalSay { get; set; }
        public string City { get; set; }
        public Image Portrait { get; set; }

        public TestForm7()
        {
            InitializeComponent();
            Hobby = new List<string>();
            ID = Guid.NewGuid();
            textBox1.Text = ID.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NickName = textBox2.Text;
            Password = textBox3.Text;
            PersonalSay = richTextBox1.Text;
            City = comboBox1.Text;

            Console.WriteLine($"ID:{ID}");
            Console.WriteLine($"NickName:{NickName}");
            Console.WriteLine($"Password:{Password}");
            Console.WriteLine($"Gender:{Gender}");
            foreach (var h in Hobby)
            {
                Console.WriteLine($"Hobby:{h}");
            }
            Console.WriteLine($"PersonalSay:{PersonalSay}");
            Console.WriteLine($"City:{City}");
            Console.WriteLine($"Portrait:{Portrait?.GetHashCode()}");
        }

        private void radioButton1_Click(object sender, EventArgs e)
        {
            Gender = 1;
        }

        private void radioButton2_Click(object sender, EventArgs e)
        {
            Gender = 0;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                if (!Hobby.Contains(checkBox1.Text))
                {
                    Hobby.Add(checkBox1.Text);
                }
            }
            else
            {
                if (Hobby.Contains(checkBox1.Text))
                {
                    Hobby.Remove(checkBox1.Text);
                }
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                if (!Hobby.Contains(checkBox2.Text))
                {
                    Hobby.Add(checkBox2.Text);
                }
            }
            else
            {
                if (Hobby.Contains(checkBox2.Text))
                {
                    Hobby.Remove(checkBox2.Text);
                }
            }
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            //打开文件选择器，得到选择的图片文件
            using (var dialog = new OpenFileDialog())
            {
                dialog.Title = "选择一张图片";
                dialog.Filter = "图片文件|*.jpg;*.png;*.jpeg;*.bmp;*.gif";
                dialog.FilterIndex = 0;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        Portrait = new Bitmap(dialog.FileName, true);
                        pictureBox1.Image = Portrait;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            }
        }

        private void TestForm7_SizeChanged(object sender, EventArgs e)
        {
            tableLayoutPanel1.Width = (int)(this.Width * 1.0 / 2);
            tableLayoutPanel1.Height = (int)(this.Height * 0.9);
        }
    }
}
