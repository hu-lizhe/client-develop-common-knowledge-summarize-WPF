﻿# 熟悉常用的控件
每个客户端框架里都自带非常多的控件（在一些框架中也叫组件，这里我们统一叫控件），
其中有一些控件是非常常用的，这一节，我将列举我认为的常用的控件，在你探索新框架的过程中，
我希望你能在新框架中找到每个与之对应的控件。  
- 显示文字：label  
- 按钮：button
- 显示图片：picturebox
- 文本输入框：textbox
- 富文本编辑器(多行时可显示滚动条，可设置字体，段落格式等)：richtextbox
- 单选框：radiobutton
- 复选框：checkbox
- 带下拉列表的文本框：combobox
- 当鼠标移到相关控件时显示信息：tooltip
- 列表：listview
- 普通容器(管理一组控件)：panel
- 表格布局容器：TableLayoutPanel
- 流式布局容器：FlowLayoutPanel

我会创建一个Demo，尽可能把以上所有控件都用一遍，你们学习新框架时，复刻一下这个Demo。  
