﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform_demo._5_控件间的数据传递方式.EventUtils
{
    public partial class TestEventForm : Form
    {
        public TestEventForm()
        {
            InitializeComponent();
            var demoA = new DemoA();
            var demoB = new DemoB();
            demoB.ShowMsg("梦在旅途", "i love csharp and java!");
        }
    }
}
