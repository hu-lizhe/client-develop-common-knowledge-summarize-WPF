﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winform_demo._5_控件间的数据传递方式.demo1
{
    /// <summary>
    /// 监考老师类
    /// </summary>
    internal class Invigilator
    {
        public delegate void NotifyEventHandler();
        public event NotifyEventHandler OnStartNotify;
        public event NotifyEventHandler OnEndNotify;

        public void StartExam()
        {
            Console.WriteLine("监考老师宣布开始考试");
            OnStartNotify?.Invoke();
        }

        public void EndExam()
        {
            Console.WriteLine("监考老师宣布结束考试");
            OnEndNotify?.Invoke();
        }
    }
}
