﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform_demo._5_控件间的数据传递方式.demo1
{
    public partial class TestForm9 : Form
    {
        public TestForm9()
        {
            InitializeComponent();
            //新建一个监考老师和十个考生
            var teacher = new Invigilator();
            List<Examinee> examines = new List<Examinee>();
            for (int i = 0; i < 10; i++)
            {
                var examinee = new Examinee(i + 1);
                //考生订阅监考老师的开始考试事件和结束考试事件
                teacher.OnStartNotify += examinee.StartExam;
                teacher.OnEndNotify += examinee.EndtExam;
                examines.Add(examinee);
            }
            //监考老师宣布考试
            teacher.StartExam();
            Thread.Sleep(1000); //睡眠一秒模拟考试时间
            teacher.EndExam();
        }
    }
}
