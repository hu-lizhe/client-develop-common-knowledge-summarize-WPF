﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winform_demo._5_控件间的数据传递方式.demo1
{
    /// <summary>
    /// 考生类
    /// </summary>
    internal class Examinee
    {
        private int id;

        public Examinee(int id)
        {
            this.id = id;
        }   

        public void StartExam()
        {
            Console.WriteLine($"考生{id}开始考试作答...");
        }

        public void EndtExam()
        {
            Console.WriteLine($"考生{id}结束考试作答，并上传考卷");
        }
    }
}
