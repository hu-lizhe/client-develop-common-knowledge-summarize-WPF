﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winform_demo._5_控件间的数据传递方式.demo2
{
    internal class Wifi
    {
        public delegate void StateChangedEventHandler(bool state);
        public event StateChangedEventHandler StateChanged;

        private bool state = false;  //false表示当前无法工作，true表示可以工作

        public void Fixing()
        {
            Console.WriteLine("Wifi维修中...");
            StateChanged?.Invoke(state);
        }
        /// <summary>
        /// 维修完成，可工作
        /// </summary>
        public void FixDone()
        {
            state = true;
            Console.WriteLine("Wifi修好了，可以工作");
            StateChanged?.Invoke(state);
        }
    }
}
