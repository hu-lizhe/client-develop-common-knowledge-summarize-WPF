﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform_demo._5_控件间的数据传递方式.demo2
{
    public partial class TestForm10 : Form
    {
        public TestForm10()
        {
            InitializeComponent();
            Wifi wifi = new Wifi();
            XiaoMing xiaoMing = new XiaoMing();
            wifi.StateChanged += xiaoMing.PlayGame;
            wifi.Fixing();
            wifi.FixDone();
        }
    }
}
