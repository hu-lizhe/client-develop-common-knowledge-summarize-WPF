﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winform_demo._5_控件间的数据传递方式.demo2
{
    internal class XiaoMing
    {
        public void PlayGame(bool wifiState)
        {
            if (wifiState == true)
            {
                Console.WriteLine("小明开始努力通关游戏");
            }
            else
            {
                Console.WriteLine("小明玩不了游戏表示很难受");
            }
        }
    }
}
