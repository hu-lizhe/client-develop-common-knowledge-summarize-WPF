﻿using System.Windows.Forms;

namespace winform_demo._5_控件间的数据传递方式.demo3
{
    public partial class TestForm12 : Form
    {
        public TestForm12()
        {
            InitializeComponent();
            dianDongFa1.ValueChanged += dianDongFa2.DianDongFa_SetValue;
            dianDongFa2.ValueChanged += dianDongFa1.DianDongFa_SetValue;
        }
    }
}
