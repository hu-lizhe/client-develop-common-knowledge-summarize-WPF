﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform_demo._5_控件间的数据传递方式.demo3
{
    public partial class TestForm11 : Form
    {
        public TestForm11()
        {
            InitializeComponent();
            //每次截止阀状态改变时通知电动阀更新自己的数据
            myButton1.StateChanged += dianDongFa1.UpdateValue;
        }

        private void myButton1_StateChanged(int state)
        {
            if (state == 0)
            {
                myButton1.Text = "截止阀已关闭";
            }
            else if (state == 1)
            {
                myButton1.Text = "截止阀已开启";
            }
        }
    }
}
