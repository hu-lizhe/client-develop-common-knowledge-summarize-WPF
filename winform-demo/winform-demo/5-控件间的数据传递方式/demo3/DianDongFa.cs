﻿using System;
using System.Windows.Forms;

namespace winform_demo._5_控件间的数据传递方式.demo3
{
    internal class DianDongFa:TrackBar
    {
        private int lastValue;

        public DianDongFa()
        {
            Minimum = 0;
            Maximum = 100;
            Value = 50;
        }

        public void UpdateValue(int state)
        {
            if (state == 0)
            {
                lastValue = Value;
                Value = 0;
            }
            else if (state == 1)
            {
                Value = lastValue;
            }
        }

        public void DianDongFa_SetValue(object sender, EventArgs e)
        {
            if (sender is DianDongFa)
            {
                if (Value != (sender as DianDongFa).Value)
                {
                    Value = (sender as DianDongFa).Value;
                }
            }
        }
    }
}
