﻿namespace winform_demo._5_控件间的数据传递方式.demo3
{
    partial class TestForm12
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dianDongFa1 = new winform_demo._5_控件间的数据传递方式.demo3.DianDongFa();
            this.dianDongFa2 = new winform_demo._5_控件间的数据传递方式.demo3.DianDongFa();
            ((System.ComponentModel.ISupportInitialize)(this.dianDongFa1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dianDongFa2)).BeginInit();
            this.SuspendLayout();
            // 
            // dianDongFa1
            // 
            this.dianDongFa1.Location = new System.Drawing.Point(205, 106);
            this.dianDongFa1.Maximum = 100;
            this.dianDongFa1.Name = "dianDongFa1";
            this.dianDongFa1.Size = new System.Drawing.Size(387, 45);
            this.dianDongFa1.TabIndex = 0;
            this.dianDongFa1.Value = 50;
            // 
            // dianDongFa2
            // 
            this.dianDongFa2.Location = new System.Drawing.Point(205, 236);
            this.dianDongFa2.Maximum = 100;
            this.dianDongFa2.Name = "dianDongFa2";
            this.dianDongFa2.Size = new System.Drawing.Size(387, 45);
            this.dianDongFa2.TabIndex = 1;
            this.dianDongFa2.Value = 50;
            // 
            // TestForm12
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dianDongFa2);
            this.Controls.Add(this.dianDongFa1);
            this.Name = "TestForm12";
            this.Text = "TestForm12";
            ((System.ComponentModel.ISupportInitialize)(this.dianDongFa1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dianDongFa2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DianDongFa dianDongFa1;
        private DianDongFa dianDongFa2;
    }
}