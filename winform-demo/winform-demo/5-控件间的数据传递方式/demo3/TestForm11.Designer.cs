﻿namespace winform_demo._5_控件间的数据传递方式.demo3
{
    partial class TestForm11
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.myButton1 = new winform_demo._4_自定义控件与用户控件.demo.MyButton();
            this.dianDongFa1 = new winform_demo._5_控件间的数据传递方式.demo3.DianDongFa();
            ((System.ComponentModel.ISupportInitialize)(this.dianDongFa1)).BeginInit();
            this.SuspendLayout();
            // 
            // myButton1
            // 
            this.myButton1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.myButton1.Location = new System.Drawing.Point(271, 109);
            this.myButton1.Name = "myButton1";
            this.myButton1.PressedColor = System.Drawing.SystemColors.ControlDark;
            this.myButton1.PressedImage = null;
            this.myButton1.Size = new System.Drawing.Size(228, 48);
            this.myButton1.State = 1;
            this.myButton1.TabIndex = 1;
            this.myButton1.Text = "截止阀已开启";
            this.myButton1.UnpressedColor = System.Drawing.SystemColors.Control;
            this.myButton1.UnpressedImage = null;
            this.myButton1.UseVisualStyleBackColor = false;
            this.myButton1.StateChanged += new winform_demo._4_自定义控件与用户控件.demo.MyButton.StateChangedEventHandler(this.myButton1_StateChanged);
            // 
            // dianDongFa1
            // 
            this.dianDongFa1.Location = new System.Drawing.Point(254, 216);
            this.dianDongFa1.Maximum = 100;
            this.dianDongFa1.Name = "dianDongFa1";
            this.dianDongFa1.Size = new System.Drawing.Size(268, 45);
            this.dianDongFa1.TabIndex = 0;
            this.dianDongFa1.Value = 50;
            // 
            // TestForm11
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.myButton1);
            this.Controls.Add(this.dianDongFa1);
            this.Name = "TestForm11";
            this.Text = "TestForm11";
            ((System.ComponentModel.ISupportInitialize)(this.dianDongFa1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DianDongFa dianDongFa1;
        private _4_自定义控件与用户控件.demo.MyButton myButton1;
    }
}