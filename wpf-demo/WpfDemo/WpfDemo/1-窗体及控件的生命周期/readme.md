﻿# 窗体及控件的生命周期

## 窗体

WPF中的窗体与winform中的窗体是两个完全不同的类，WPF中的窗体叫Window类，
继承自ContentControl。

窗体构建到显示的事件顺序：  
Window_Initialized  
在FrameworkElement初始化时发生  
Window_SourceInitialized  
在窗口的HwndSource对象创建之后，显示窗口之前发生  
Window_Activated  
在窗口成为前台窗口时发生  
Window_Loaded  
在FrameworkElement布局，呈现并准备开始交互时发生  
Window_ContentRendered  
在窗口的内容呈现完毕之后发生  

窗体销毁时的事件顺序：  
Window_Closing  
在调用Close之前发生
Window_Closed  
在窗口即将关闭时发生  

什么是FrameworkElement？  
官方解释：  
Provides a WPF framework-level set of properties, events, and methods for Windows Presentation Foundation (WPF) elements. This class represents the provided WPF framework-level implementation that is built on the WPF core-level APIs that are defined by UIElement.

翻译：  
为Windows Presentation Foundation（WPF）元素提供一组WPF框架级别的属性、事件和方法。此类表示所提供的WPF框架级实现，该实现构建在UIElement定义的WPF核心级API上。

FrameworkElement有Loaded事件和Initialized事件。
Initialized事件的执行时机是在当前元素的所有属性已经被设置完毕。
通常情况下，在一个xaml树中，子元素的Init事件会先于父元素触发。当子元素Init事件执行完毕后其IsInitialized属性变为true，并告知父元素，
父元素再开始执行设置属性，触发Init事件并将IsInitialized改为true，如此往复。  

Load事件的执行时机是在Initialized之后，也就是说此时属性肯定都是设置完了的，但是此时有些属性是不准确的，例如ActualWidth和 ActualHeight，
在Load事件触发之后，即将绘制界面。  
Loaded事件通常会在第一次Render之前触发，Element已经被Layout，数据 绑定也已经被计算。需要注意的是如果你在Loaded中Invalidate了Layout，在Render之前该事件有可能会被重新触发。  

## 控件  
