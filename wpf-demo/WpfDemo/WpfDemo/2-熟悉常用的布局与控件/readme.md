﻿# 熟悉常用的布局与控件

## 常用布局

wpf的布局有五种大类

- DockPanel 停靠面板
- StackPanel 栈面板
- WrapPanel 环绕面板
- Grid  网格面板
- Canvas   精准定位

### DockPanel

DockPanel 面板，里面的元素用Dock属性来设置停靠在哪个方向,分别有:Right(右停靠),Left(左停靠)，Buttom（下停靠）,Top（上停靠）
```
<DockPanel LastChildFill="true">
    <Button DockPanel.Dock="Left" Content="左停靠"/>
    <Button DockPanel.Dock="Right" Content="右停靠"/>
    <Button DockPanel.Dock="Bottom" Content="下停靠"/>
    <Button DockPanel.Dock="Top" Content="上停靠"/>
    <Button DockPanel.Dock="Left" Content="左停靠"/>
</DockPanel>
```
LastChildFill属性表明，最后一个参数是否撑满整个容器，默认为true。  

### StackPanel

StackPanel栈面板，特征是每个元素占一行或一列，不会换行，可根据Orientation属性，设置Vertical竖向排列，或者Horizontal横向排列，默认值是Vertical。在这个面板中你可以把一项一项元素按照方向堆叠。
而且默认元素都是居中的。

```
<StackPanel Orientation="Horizontal">
    <Button Content="横向1" Width="200"/>
    <Button Content="横向2" Width="100"/>
    <Button Content="横向3"/>
    <Button Content="横向4"/>
</StackPanel>
```

### WrapPanel

WrapPanel，它和上面的StackPanel很像，但WrapPanel里面的元素如果超出了长度或者宽度，会换行。同样它也是靠Orientation属性来设置是Horizontal（从左往右排列，超出向下换行），或者Vertical（从上往下排列，从左往右换行）
WrapPanel默认元素是在左/上的。

```
<WrapPanel Orientation="Vertical">
    <Button Content="横向1" Width="100" Height="100"/>
    <Button Content="横向2" Width="100" Height="100"/>
    <Button Content="横向3" Width="100" Height="100"/>
    <Button Content="横向4" Width="100" Height="100"/>
    <Button Content="横向4" Width="100" Height="100"/>
    <Button Content="横向4" Width="100" Height="100"/>
    <Button Content="横向4" Width="100" Height="100"/>
    <Button Content="横向4" Width="100" Height="100"/>
    <Button Content="横向4" Width="100" Height="100"/>
</WrapPanel>
```

### Grid

Grid,网格式布局，它的特征是将页面分成一个一个小格子，然后将控件放在格子里面。它是所有布局里面最复杂，但是应用最广泛的布局。
Grid的主要属性为：ColumnDefinitions，RowDefinitions，表明有几行和几列，ShowGridLines属性是表明是否显示网格线，默认是false。控件用Grid.Column来配置在第几列，Grid.Row来配置在第几行。

```
<Grid ShowGridLines="True">
    <Grid.ColumnDefinitions>
        <ColumnDefinition/>
        <ColumnDefinition/>
        <ColumnDefinition/>
    </Grid.ColumnDefinitions>
    <Grid.RowDefinitions>
        <RowDefinition/>
        <RowDefinition/>
        <RowDefinition/>
    </Grid.RowDefinitions>
    <Button Content="button1" Grid.Column="0" Grid.Row="0"/>
    <Button Content="button2" Grid.Column="1" Grid.Row="1"/>
    <Button Content="button3" Grid.Column="2" Grid.Row="2"/>
</Grid>
```

### Canvas

Canvas布局，一个类似坐标系的面板,通过left,right,right,bottom 四个字段来设置控件的摆放位置。
如果设置了left或者top，则之后在设置right或者bottom是无效的。  

```
<Canvas>
    <Button Content="Test1" Canvas.Left="20" Canvas.Top="20"  Width="60"/>
    <Button Content="Test2" Canvas.Right="50" Canvas.Bottom="200" Width="60"/>
    <Button Content="Test3" Canvas.Left="150" Canvas.Top="50" Width="60"/>
    <Button Content="Test4" Canvas.Right="150" Canvas.Bottom="50"  Width="60"/>
    <Button Content="Test5" Canvas.Left="150" Canvas.Right="150" Canvas.Top="150" Canvas.Bottom="150" />
</Canvas>
```

canvas还有一个z_index属性来设置控件的折叠顺序，如果不设置z_index，系统是按照控件定义的先后顺序来排序，定义了z_index属性，可调整控件的重叠顺序

```
<Canvas>
    <Button Content="Test1" Panel.ZIndex="3"  Canvas.Left="150" Canvas.Top="60"  Width="90" Height="50" Background="Yellow" Foreground="Gray"/>
    <Button Content="Test2" Panel.ZIndex="2" Canvas.Left="150" Canvas.Top="80" Width="90"  Height="50" Background="Red" Foreground="White"/>
    <Button Content="Test3" Panel.ZIndex="1" Canvas.Left="150" Canvas.Top="100" Width="90"  Height="50" Background="green" Foreground="White"/>
    <Button Content="Test4" Panel.ZIndex="0" Canvas.Left="150" Canvas.Top="120"  Width="90"  Height="50" />
</Canvas>
```

## 控件

label => textblock
textbox=>textbox

## 一些问题

1. XAML中x:name和name有什么区别？  
对于FrameworkElement, VisualStateGroup这种类型来说，x:name和name是一样的。
XAML中的所有类型都有x:name属性，这个属性用来唯一标识XAML中的对象，以便后台代码中能找到对应的对象。
FrameworkElement, VisualStateGroup这种类型本身具有name属性，然后还带有[RuntimeNameProperty(“Name”)]的特性，
这个特性会把name映射给x:name。