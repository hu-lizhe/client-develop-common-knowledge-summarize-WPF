﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media.Imaging;

namespace WpfDemo._2_熟悉常用的布局与控件
{
    /// <summary>
    /// Window2.xaml 的交互逻辑
    /// </summary>
    public partial class Window2 : Window
    {
        public Window2()
        {
            InitializeComponent();
            var cities = new List<string>();
            cities.Add("上海");
            cities.Add("北京");
            cities.Add("天津");
            cities.Add("重庆");
            cbbox_city.ItemsSource = cities;
        }

        private void bd_portrait_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Title = "选择图片文件";
            dialog.Filter = "Image files (*.jpg)|*.jpg|All Files (*.*)|*.*";
            dialog.FilterIndex = 0;
            string imgPath = "";
            if (dialog.ShowDialog() == true)
            {
                imgPath = dialog.FileName;
            }
            if (!string.IsNullOrEmpty(imgPath))
            {
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(imgPath);
                bitmap.EndInit();

                img_portrait.Source = bitmap;
            }
        }

        private void btn_submit_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine(txt_account.Text);
            Console.WriteLine(txt_name.Text);
            Console.WriteLine(txt_pwd.Password);
            Console.WriteLine($"{rb_man.Content}:{rb_man.IsChecked}");
            Console.WriteLine(rb_woman.IsChecked);
            Console.WriteLine(chkbox_ball.IsChecked);
            Console.WriteLine(chkbox_rap.IsChecked);
            TextRange textRange = new TextRange(rtxt_sign.Document.ContentStart, rtxt_sign.Document.ContentEnd);
            Console.WriteLine(cbbox_city.Text);
            Console.WriteLine(img_portrait.Source);
        }
    }
}
