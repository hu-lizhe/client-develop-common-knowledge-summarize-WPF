﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfDemo._4_了解binding
{
    /// <summary>
    /// Window5.xaml 的交互逻辑
    /// </summary>
    public partial class Window5 : Window
    {
        Student stu;

        public Window5()
        {
            InitializeComponent();

            stu = new Student();
            //准备binding
            Binding binding = new Binding();
            binding.Source = stu;
            binding.Path = new PropertyPath("Name");
            //使用binding连接数据源与目标UI
            BindingOperations.SetBinding(txt,TextBox.TextProperty, binding);
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            stu.Name += "123";
        }
    }
}
