﻿using System;
using System.Windows;

namespace WpfDemo
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
           // Console.WriteLine("Window_Initialized");
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            //Console.WriteLine("Window_Activated");
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            //Console.WriteLine("Window_ContentRendered");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Console.WriteLine("Window_Loaded");
        }

        private void Window_SourceInitialized(object sender, EventArgs e)
        {
            //Console.WriteLine("Window_SourceInitialized");
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            //Console.WriteLine("Window_Unloaded");
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Console.WriteLine("Window_Closing");
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            //Console.WriteLine("Window_Closed");
        }
    }
}
