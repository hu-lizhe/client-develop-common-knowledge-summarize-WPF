﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfDemo._3_熟悉Xaml
{
    public class DataConverter: TypeConverter
    {
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var strs = value.ToString().Split(':');
            if (strs.Length > 1)
            {
                var data = new Data();
                data.ID = int.Parse(strs[0]);
                data.Name = strs[1];
                return data;
            }
            return base.ConvertFrom(context, culture, value);
        }
    }
}
