﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfDemo._3_熟悉Xaml
{

    public class DataText:TextBlock
    {
        public Data data { get; set; }
    }

    [TypeConverter(typeof(DataConverter))]
    public class Data
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
