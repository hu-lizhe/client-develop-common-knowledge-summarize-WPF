﻿## 给对象属性赋值

XAML中为对象属性赋值有两种语法：  
1. 使用字符串进行简单赋值
2. 使用属性元素进行复杂赋值

### 使用字符串进行简单赋值

以window3.xaml为例，我们创建一个矩形，并以某种颜色填充，
rectangle有个fill属性，它支持常用颜色的字符串如blue，或者颜色的十六进制表示如#ccc。

但在某些情况下，一些属性的值是比较特殊或复杂的，比如是一个控件实例的引用，或者某个自定义枚举类型。
这时候就需要自定义一个继承自TypeConverter的类来帮助Xaml按照我们想要的
方式去处理那些属性值。  

#### 使用TypeConverter将XAML的attribute标签与对象的属性进行映射

举个例子，我们写一个类DataText，继承自TextBlock，添加一个自定义类型的类Data属性data，
这个Data里有一个ID和一个name，接着在window3.xaml中我们声明一个DataText，用attr标签赋值的方式
给data属性赋值："36:hello"，其中我们希望36是ID，hello是name，那么该怎么做呢？

首先我们要写一个DataConverter继承自TypeConverter，重写其ConvertFrom方法，其中value参数就是xaml中的这个属性收到的值，
我们就是在这里拿到那个值进行处理：
```
 var strs = value.ToString().Split(':');
if (strs.Length > 1)
{
    var data = new Data();
    data.ID = int.Parse(strs[0]);
    data.Name = strs[1];
    return data;
}
return base.ConvertFrom(context, culture, value);
```

这还不够，接着我们要在Data这个类上写上特性：
```
[TypeConverter(typeof(DataConverter))]
public class Data
{
    public int ID { get; set; }
    public string Name { get; set; }
}
```

这样，当我们点击window3.xaml按钮时，就能看到id和name的输出了。

### 使用属性元素进行复杂赋值

比如设置网格布局时，Grid.ColumnDefinitions和Grid.RowDefinitions就是属性元素，
通常用属性元素进行赋值的，值的形式都比较复杂，比如是一个集合，或者是结构体，类之类的。

## 标记扩展

标记扩展是一种特殊的attribute = value的用法，其特殊的地方在于value字符串是一对由花括号及其括起来的内容组成。
当XAML编译器遇到花括号时会做特殊处理。

### 数据绑定

在属性值中使用{Binding ElementName=..._input,Path=...,Mode=...}  
ElementName为数据源所在的控件名称，Path为你要绑定的数据源属性，mode为绑定模型（单向或双向）



## 如何导入程序集和引用其中的名称空间

xmlns:映射名="clr-namespace:类库中名称空间的名字;assembly=类库文件名"  
当引用同一程序集的名称空间时，后面的assembly可省略。  


